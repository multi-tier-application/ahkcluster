terraform {
  required_version = ">=1.0"

  required_providers {
    azapi = {
      source  = "azure/azapi"
      version = "~>1.5"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~>3.0"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.9.1"
    }
  }
}

provider "azurerm" {
  features {}
  client_id       = "ba3e8a34-13fa-4259-80cf-84dfbe63425c"
  client_secret   = var.client_secret
  tenant_id       = "1269359b-e32f-474c-9d79-90e89be50de6"
  subscription_id = "977466de-b86c-4788-b2dc-ecc1c74632ce"

}